﻿using System.Diagnostics;

namespace HEMTT.Utils
{
    public static class StopwatchExtensions
    {
        public static readonly string[] TimeSuffixes =
                   { "ms", "s", "m" };
        public static string TimeSuffix(this Stopwatch value, int decimalPlaces = 1)
        {
            long time = value.ElapsedMilliseconds;
            if (decimalPlaces < 0) { throw new ArgumentOutOfRangeException("decimalPlaces"); }
            if (time == 0) { return string.Format("{0:n" + decimalPlaces + "} bytes", 0); }

            // mag is 0 for bytes, 1 for KB, 2, for MB, etc.
            int mag = (int)Math.Log(time, 1000);

            // 1L << (mag * 10) == 2 ^ (10 * mag) 
            // [i.e. the number of bytes in the unit corresponding to mag]
            decimal adjustedSize = (decimal)time / (1L << (mag * 10));

            // make adjustment when the value is large enough that
            // it would round up to 1000 or more
            if (Math.Round(adjustedSize, decimalPlaces) >= 1000)
            {
                mag += 1;
                adjustedSize /= 1000;
            }

            return string.Format("{0:n" + decimalPlaces + "} {1}",
                adjustedSize,
                TimeSuffixes[mag]);
        }
    }
}
