﻿using HEMTT.Utils;

namespace HEMTT.HEMTTPbo
{
    [Serializable]
    public class FileEntry
    {
        #region Fields
        public string _fileName;
        public PackingType PackingMethod = PackingType.Uncompressed;
        public ulong OriginalSize;
        public ulong TimeStamp;
        public ulong StartOffest;
        public ulong DataSize;
        public byte[] OrgName;
        [NonSerialized]
        public readonly PboArchive ParentArchive; 
        #endregion

        public string FileName
        {
            get
            {
                return _fileName;
            }
            set { _fileName = value; }
        }

        public override string ToString()
        {
            return string.Format("{0} ({1})", _fileName, OriginalSize.SizeSuffix()) ;
        }


        public FileEntry(string filename, ulong type, ulong osize, ulong timestamp, ulong datasize, ulong startOffset = 0x0)
        {
            _fileName = filename;
            switch (type)
            {
                case 0x0:
                    PackingMethod = PackingType.Uncompressed;
                    break;
                case 0x43707273:
                    PackingMethod = PackingType.Packed;
                    break;
                case 0x56657273:
                    PackingMethod = PackingType.Uncompressed;
                    break;
                case 0x456e6372:
                    PackingMethod = PackingType.Encrypted;
                    break;
            }
            OriginalSize = osize;
            TimeStamp = timestamp;
            DataSize = datasize;
            StartOffest = startOffset;
        }

        public FileEntry(PboArchive parent, string filename, ulong type, ulong osize, ulong timestamp, ulong datasize, byte[] file, ulong startOffset = 0x0)
        : this(filename, type, osize, timestamp, datasize)
        {
            ParentArchive = parent;
            OrgName = file;
        }

        public bool Extract(string outPath)
        {
            if (ParentArchive is null)
                throw new Exception("No parent Archive");
            if (!Directory.Exists(Path.GetDirectoryName(outPath)))
                Directory.CreateDirectory(Path.GetDirectoryName(outPath));
            return ParentArchive.Extract(this, outPath);
        }

        public Stream Extract()
        {
            if (ParentArchive is null)
                throw new Exception("No parent Archive");
            return ParentArchive.Extract(this);
        }

    }
}
