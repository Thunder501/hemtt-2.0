﻿using System.Globalization;
using System.Text;

namespace HEMTT.HEMTTPbo
{
    internal class PboUtilities
    {
        internal static string GetrelativePath(string fileSpec, string folder)
        {
            Uri pathUri = new(fileSpec);

            if (!folder.EndsWith(Path.DirectorySeparatorChar.ToString(CultureInfo.InvariantCulture)))
                folder += Path.DirectorySeparatorChar;
            Uri folderUri = new(folder);
            return Uri.UnescapeDataString(folderUri.MakeRelativeUri(pathUri).ToString().Replace('/', Path.DirectorySeparatorChar));
        }

        #region Write
        internal static void WriteASIIZ(FileStream stream, byte[] fileName)
        {
            byte[] copy = new byte[fileName.Length + 1];
            fileName.CopyTo(copy, 0);
            copy[fileName.Length] = 0x0;
            stream.Write(copy, 0, copy.Length);
        }

        internal static void WriteString(FileStream stream, string str)
        {
            byte[] buffer = Encoding.UTF8.GetBytes(str + "\0");
            stream.Write(buffer, 0, buffer.Length);
        }

        internal static void WriteLong(FileStream stream, long num)
        {
            byte[] buffer = BitConverter.GetBytes(num);
            stream.Write(buffer, 0, 4);
        }
        #endregion

        #region Read
        internal static byte[] ReadStringArray(FileStream stream)
        {
            List<byte> list = new();
            while (true)
            {
                byte ch = (byte)stream.ReadByte();
                if (ch == 0x0)
                    break;
                list.Add(ch);
            }

            return list.ToArray();
        }

        internal static ulong ReadLong(FileStream stream)
        {
            byte[] buffer = new byte[4];
            stream.Read(buffer, 0, 4);
            return BitConverter.ToUInt32(buffer, 0);
        }

        internal static string ReadString(FileStream stream)
        {
            string str = "";
            while (true)
            {
                byte ch = (byte)stream.ReadByte();
                if (ch == 0x0)
                    break;
                str += (char)ch;
            }
            return str;
        } 
        #endregion
    }
}