﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HEMTT.HEMTTPbo
{
    public class ProductEntry
    {
        #region Fields
        private string _name;
        private string _prefix;
        private string _productVersion;
        private List<string> _additional = new();
        #endregion

        #region Properties
        public string Name { get => _name; set => _name = value; }
        public string Prefix { get => _prefix; set => _prefix = value; }
        public string ProductVersion { get => _productVersion; set => _productVersion = value; }
        public List<string> Additional { get => _additional; set => _additional = value; }
        #endregion

        #region Constructors
        public ProductEntry()
        {
            _name = _prefix = _productVersion = "";
            Additional = new();
        }

        public ProductEntry(string name, string prefix, string productVersion, List<string> addList = null)
        {
            Name = name;
            Prefix = prefix;
            ProductVersion = productVersion;
            if (addList is not null)
                Additional = addList;
        } 
        #endregion


    }
}
