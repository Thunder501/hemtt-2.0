﻿using Serilog;
using System.Security.Cryptography;
using System.Text;
using System.Diagnostics;

namespace HEMTT.HEMTTPbo
{
    public enum PackingType
    {
        Uncompressed,
        Packed,
        Encrypted
    }
    public class PboArchive : IDisposable
    {
        #region Fields
        private ProductEntry _productEntry = new ProductEntry("", "", "", new List<string>());
        private List<FileEntry> _files = new();
        private string _path;
        private long _dataStart;
        private FileStream _stream;
        private byte[] _checksum;
        private static byte[] _file; 
        private static readonly Stopwatch _watch = new();
        #endregion

        #region Readonly
        private static readonly List<char> InvalidFile = Path.GetInvalidFileNameChars().ToList();
        private static readonly List<char> InvalidPath = Path.GetInvalidFileNameChars().ToList();
        private static readonly List<char> _literalList = new() { '\'', '\"', '\\', '\0', '\a', '\b', '\f', '\n', '\r', '\t', '\v' };
        private static readonly ILogger Logger = Log.Logger.ForContext<PboArchive>();
    #endregion

        #region Properties
        public List<FileEntry> Files { get { return _files; } }

        public ProductEntry ProductEntry { get { return _productEntry; } }

        public byte[] Checksum { get { return _checksum; } }

        public string PboPath { get { return _path; } }

        public long DataStart { get { return _dataStart; } }
        #endregion

        public PboArchive(string path, bool close = true)
        {
            if (!File.Exists(path))
                throw new FileNotFoundException("File not found");
            _path = path;
            _stream = new(path, FileMode.Open, FileAccess.Read, FileShare.Read, 8, FileOptions.SequentialScan);
            if (_stream.ReadByte() != 0x0)
                return;
            if (!ReadHeader(_stream))
                _stream.Position = 0;
            while (true)
                if (!ReadEntry(_stream))
                    break;
            _dataStart = _stream.Position;
            ReadChecksum(_stream);
            if (close)
            {
                _stream.Dispose();
                _stream = null;
            }
        }


        public static bool Create(string directoryPath, string outPath)
        {
            _watch.Start();
            var dir = new DirectoryInfo(directoryPath);
            if (!dir.Exists)
                throw new DirectoryNotFoundException();
            directoryPath = dir.FullName;
            var entry = new ProductEntry("prefix", "", "", new List<string>());
            var files = Directory.GetFiles(directoryPath, "$*$");
            Logger.Verbose("Getting misc Data");
            foreach (var file in files)
            {
                var varname = Path.GetFileNameWithoutExtension(file).Trim('$');
                var data = File.ReadAllText(file).Split('\n')[0];
                switch (varname.ToLowerInvariant())
                {
                    case "pboprefix":
                        entry.Prefix = data;
                        break;
                    case "prefix":
                        entry.Prefix = data;
                        break;
                    case "version":
                        entry.ProductVersion = data;
                        break;
                    default:
                        entry.Additional.Add(data);
                        break;
                }
            }
            return Create(directoryPath, outPath, entry);
        }

        private static bool Create(string directoryPath, string outPath, ProductEntry productEntry)
        {
            if (!_watch.IsRunning)
                _watch.Start();
            var dir = new DirectoryInfo(directoryPath);
            if (!dir.Exists)
                throw new DirectoryNotFoundException();
            directoryPath = dir.FullName;
            var files = Directory.GetFiles(directoryPath, "*", SearchOption.AllDirectories);
            Logger.Verbose("Getting all files");
            var entries = new List<FileEntry>();
            foreach (var file in files)
            {
                if (Path.GetFileName(file).StartsWith("$") && Path.GetFileName(file).EndsWith("$"))
                    continue;
                FileInfo info = new(file);
                string path = PboUtilities.GetrelativePath(info.FullName, directoryPath);
                entries.Add(new(path, 0x0, (ulong)info.Length, (ulong)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1)).TotalSeconds), (ulong)info.Length));
            }
            try
            {
                if (File.Exists(outPath))
                {
                    string newFile = outPath + ".bak";
                    File.Move(outPath, newFile, true);
                    Logger.Verbose("Backing up old pbo");
                }
                using var stream = File.Create(outPath);
                stream.WriteByte(0x0);
                Logger.Verbose("Writing header");
                WriteProductEntry(productEntry, stream);
                stream.WriteByte(0x0);
                entries.Add(new FileEntry(null, "", 0, 0, 0, 0, _file));
                foreach (var entry in entries)
                {
                    Logger.Verbose("Writing file header: {entry}", entry);
                    WriteFileEntry(stream, entry);
                }
                entries.Remove(entries.Last());
                foreach (var entry in entries)
                {
                    var buffer = new byte[2949120];
                    using var open = File.OpenRead(Path.Combine(directoryPath, entry.FileName));
                    var read = 4324324;
                    while (read > 0)
                    {
                        read = open.Read(buffer, 0, buffer.Length);
                        Logger.Verbose("Writing file data: {entry}", entry);
                        stream.Write(buffer, 0, read);
                    }
                }
                stream.Position = 0;
                byte[] hash;
                using SHA1 sha1 = SHA1.Create();
                hash = sha1.ComputeHash(stream);
                Logger.Verbose("Writing checksum");
                stream.WriteByte(0x0);
                stream.Write(hash, 0, 20);
            }
            catch (Exception ex)
            {
                Logger.Error(ex, "Creating pbo failed");
                return false;
            }
            _watch.Stop();
            string pboName = new DirectoryInfo(directoryPath).Name;
            Logger.Information("[{pboName}] built in {ElapsedMilliseconds} ms", pboName, _watch.ElapsedMilliseconds);
            return true;
        }

        private static bool IsLiteral(char ch) => _literalList.Contains(ch);

        public static string SterilizePath(string path)
        {
            var arr = Path.GetDirectoryName(path).ToCharArray();
            var builder = new StringBuilder(arr.Count());
            string dirPath = Path.GetDirectoryName(path);
            for(int i = 0; i < dirPath.Length; i++)
            {
                if (!InvalidPath.Contains(path[i]) && path[i] != Path.AltDirectorySeparatorChar)
                    builder.Append(path[i]);
                if (path[i] == Path.AltDirectorySeparatorChar)
                    builder.Append(Path.DirectorySeparatorChar);
            }
            var fileName = Path.GetFileName(path).ToCharArray();
            for (int i = 0; i < fileName.Length; i++)
            {
                var ch = fileName[i];
                if (!InvalidPath.Contains(ch) && ch != '*' && !IsLiteral(ch))
                    continue;
                fileName[i] = ((char)(Math.Min(90, 65 + ch % 5)));
            }
            return Path.Combine(builder.ToString(), new string(fileName));
        }

        private static void WriteFileEntry(FileStream stream, FileEntry entry)
        {
            if (entry.OrgName != null)
                PboUtilities.WriteASIIZ(stream, entry.OrgName);
            else
                PboUtilities.WriteString(stream, entry.FileName);
            long packing = 0x0;
            switch (entry.PackingMethod)
            {
                case PackingType.Packed:
                    packing = 0x43707273;
                    break;
                case PackingType.Encrypted:
                    packing = 0x456e6372;
                    break;
            }
            PboUtilities.WriteLong(stream, packing);
            PboUtilities.WriteLong(stream, (long)entry.OriginalSize);
            PboUtilities.WriteLong(stream, (long)entry.StartOffest);
            PboUtilities.WriteLong(stream, (long)entry.TimeStamp);
            PboUtilities.WriteLong(stream, (long)entry.DataSize);
        }

        private static void WriteProductEntry(ProductEntry productEntry, FileStream stream)
        {
            PboUtilities.WriteString(stream, "sreV");
            stream.Write(new byte[15], 0, 15);
            if (!string.IsNullOrEmpty(productEntry.Name))
                PboUtilities.WriteString(stream, productEntry.Name);
            else
                return;
            if (!string.IsNullOrEmpty(productEntry.Prefix))
                PboUtilities.WriteString(stream, productEntry.Prefix);
            else
                return;
            if (!string.IsNullOrEmpty(productEntry.ProductVersion))
                PboUtilities.WriteString(stream, productEntry.ProductVersion);
            else
                return;
            foreach (var str in productEntry.Additional)
            {
                PboUtilities.WriteString(stream, str);
            }
        }

        private void ReadChecksum(FileStream stream)
        {
            var pos = DataStart + Files.Sum(fileEntry => (long)fileEntry.DataSize) + 1;
            stream.Position = pos;
            _checksum = new byte[20];
            stream.Read(Checksum, 0, 20);
            stream.Position = DataStart;
        }

        private bool ReadEntry(FileStream stream)
        {
            var file = PboUtilities.ReadStringArray(stream);
            var filename = Encoding.UTF8.GetString(file).Replace("\t", "\\t");

            var packing = PboUtilities.ReadLong(stream);

            var size = PboUtilities.ReadLong(stream);

            var startOffest = PboUtilities.ReadLong(stream);

            var timeStamp = PboUtilities.ReadLong(stream);
            var dataSize = PboUtilities.ReadLong(stream);
            var entry = new FileEntry(this, filename, packing, size, timeStamp, dataSize, file, startOffest);
            if(entry.FileName == "")
            {
                entry.OrgName = new byte[0];
                return false;
            }
            Files.Add(entry);
            return true;
        }

        private bool ReadHeader(FileStream stream)
        {
            // TODO: Fix is so broken
            string str = PboUtilities.ReadString(stream);
            if (str != "sreV")
                return false;
            int count = 0;
            while (count < 15)
            {
                stream.ReadByte();
                count++;
            }
            List<string> list = new();
            string pboName = "";
            string version = "";
            string prefix = PboUtilities.ReadString(stream);
            if (!string.IsNullOrEmpty(prefix))
            {
                pboName = PboUtilities.ReadString(stream);
                if (!string.IsNullOrEmpty(pboName))
                {
                    version = PboUtilities.ReadString(stream);
                    if (!string.IsNullOrEmpty(version))
                        while(stream.ReadByte() != 0x0)
                        {
                            stream.Position--;
                            string s = PboUtilities.ReadString(stream);
                            list.Add(s);
                        }
                }
            }
            _productEntry = new(prefix, pboName, version, list);

            return true;
        }
        public bool ExtractAll(string outPath)
        {
            if (!Directory.Exists(outPath))
                Directory.CreateDirectory(outPath);
            byte[] buffer = new byte[10000000];
            int files = 0;
            //using FileStream prefix = File.Create(Path.Combine(outPath, "$PBOPREFIX$"));
            //byte[] buf = Encoding.UTF8.GetBytes(_productEntry.Prefix);
            //prefix.Write(buf, 0, buf.Length);
            foreach (var file in Files)
            {
                Stream stream = GetFileStream(file);
                Logger.Debug("File Start: {file}", file);
                files++;
                ulong totalRead = file.DataSize;
                string pboPath = Path.Combine(outPath, file.FileName);
                if (!Directory.Exists(Path.GetDirectoryName(pboPath)))
                    Directory.CreateDirectory(Path.GetDirectoryName(pboPath));
                using Stream outFile = File.Create(pboPath);
                while (totalRead > 0)
                {
                    int read = stream.Read(buffer, 0, (int)Math.Min(10000000, totalRead));
                    if (read <= 0)
                        return true;
                    outFile.Write(buffer, 0, read);
                    totalRead -= (ulong)read;
                }
                Logger.Debug("File End {files}", files);
            }
            return true;
        }

        public bool Extract(FileEntry fileEntry, string outPath)
        {
            if (string.IsNullOrEmpty(outPath))
                throw new NullReferenceException("Is null or empty");
            Stream mem = GetFileStream(fileEntry);
            if (mem is null)
                throw new Exception("WTF no stream");
            if (!Directory.Exists(Path.GetDirectoryName(outPath)))
                Directory.CreateDirectory(Path.GetDirectoryName(outPath));
            ulong totalRead = fileEntry.DataSize;
            using Stream outFile = File.OpenWrite(outPath);
            byte[] buffer = new byte[2949120];
            while (totalRead > 0)
            {
                int read = mem.Read(buffer, 0, (int)Math.Min(249120, totalRead));
                outFile.Write(buffer, 0, read);
                totalRead -= (ulong)read;
            }
            outFile.Close();
            mem.Close();
            return true;
        }

        private Stream GetFileStream(FileEntry fileEntry)
        {
            if (_stream is not null)
            {
                _stream.Position = (long)GetFileStreamPos(fileEntry);
                return _stream;
            }
            var mem = File.OpenRead(PboPath);
            mem.Position = (long)GetFileStreamPos(fileEntry);
            return mem;
        }

        private ulong GetFileStreamPos(FileEntry fileEntry)
            => Files.TakeWhile(e => e != fileEntry).Aggregate((ulong)DataStart, (c, e) => c + e.DataSize);


        public Stream Extract(FileEntry fileEntry)
            => GetFileStream(fileEntry);

        public void Dispose()
        {
            _stream?.Dispose();
        }
    }
}
