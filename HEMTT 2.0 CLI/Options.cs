﻿using CommandLine;

namespace HEMTT.CLI.Options
{
    internal class OptionsBase
    {
        [Option('v', "Verbose", Required = false)]
        public bool Verbose { get; set; }
        [Option('d', "Debug", Required = false)]
        public bool Debug { get; set; }
        [Option('w', "Warning")]
        public bool Warn { get; set; }
        [Option("Time")]
        public bool Time { get; set; }
    }

    [Verb("build", HelpText = "Build the project")]
    internal class BuildOptions : OptionsBase
    {
        [Option('f', Required = false)]
        public bool Force { get; set; }
        [Option]
        public IEnumerable<string> Skip { get; set; }
    }
}
