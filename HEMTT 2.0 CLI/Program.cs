﻿using Serilog;
using Serilog.Core;
using Serilog.Events;
using Serilog.Templates;
using HEMTT.HEMTTPbo;
using HEMTT.Utils;
using HEMTT.CLI.Options;
using CommandLine;
using CommandLine.Text;
using System.Diagnostics;

namespace HEMTT.CLI
{
    public class Program
    {
        private static readonly Stopwatch _watch = new Stopwatch();

        static void Main(string[] args)
        {
            var levelSwitch = new LoggingLevelSwitch(LogEventLevel.Information);
            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.ControlledBy(levelSwitch)
                .WriteTo.Console(new ExpressionTemplate(
                    "{@t:HH:mm:ss} | [{@l:u4}] | {SourceContext:l} | {@m}\n{@x}", theme: Serilog.Templates.Themes.TemplateTheme.Code))
                .CreateLogger();
            var parser = new Parser(with => with.HelpWriter = null);
            var parserResult = parser.ParseArguments<OptionsBase, BuildOptions>(args);
            parserResult
                .WithParsed<OptionsBase>(opts =>
                {
                    if (opts.Verbose)
                        levelSwitch.MinimumLevel = LogEventLevel.Verbose;
                    else if (opts.Debug)
                        levelSwitch.MinimumLevel = LogEventLevel.Debug;
                    else if (opts.Warn)
                        levelSwitch.MinimumLevel = LogEventLevel.Warning;
                    if (opts.Time)
                        _watch.Start();
                })
                .WithParsed<BuildOptions>(opt =>
                {
                    PboArchive.Create($"{Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)}\\test\\Aux61st_stamina\\", $"{Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)}\\test\\Aux61st_stamina.pbo");
                })
                .WithNotParsed(errs => DisplayHelp(parserResult, errs));

            System.Threading.Thread.Sleep(1500);
            _watch.Stop();
            Log.Logger.Information("Total execution took: {ElapsedMilliseconds}", _watch.TimeSuffix(3));
            Log.CloseAndFlush();




            //PboArchive test = new($"{Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)}\\test\\Aux61st_stamina.pbo");
            //Log.Debug("Files: {Files}", test.Files);
            //test.ExtractAll($"{Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)}\\test\\Aux61st_stamina\\");
        }

        static void DisplayHelp<T>(ParserResult<T> result, IEnumerable<Error> errs)
        {
            var helpText = HelpText.AutoBuild(result, h =>
            {
                h.AdditionalNewLineAfterOption = false;
                h.AutoVersion = false;
                return HelpText.DefaultParsingErrorsHandler(result, h);
            });
            Console.WriteLine(helpText);
        }
    }
}